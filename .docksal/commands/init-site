#!/usr/bin/env bash

#: exec_target = cli

## Initialize/reinstall site
##
## Usage: fin init-site

# Abort if anything fails
set -e

#-------------------------- Settings --------------------------------

# PROJECT_ROOT and DOCROOT are set as env variables in cli
SITE_DIRECTORY="default"
DOCROOT_PATH="${PROJECT_ROOT}/${DOCROOT}"
SITEDIR_PATH="${DOCROOT_PATH}/sites/${SITE_DIRECTORY}"

#-------------------------- END: Settings --------------------------------

#-------------------------- Functions --------------------------------

source ${PROJECT_ROOT}/.docksal/utils/functions

# Fix file/folder permissions
fix_permissions ()
{
  echo "Making site directory writable..."
  chmod 755 "${SITEDIR_PATH}"
}

# Install site
site_install ()
{
  cd "${DOCROOT_PATH}"

  call_hook "site-install-start"

  # Currently the two hooks surrounding this line are the same.
  # They are left as two for two reasons:
  #   1. It allows a clear distinction when writing them what is start and finish.
  #   2. It allows us to add here any all-project actions that need to be done in the future,
  #      without breaking backwards-compatibility.

  call_hook "site-install-finish"
}

#-------------------------- END: Functions --------------------------------

#-------------------------- Execution --------------------------------

# Project initialization steps
fix_permissions
time site_install

#-------------------------- END: Execution --------------------------------
