
# Various microserve project-specific configuration variables.
#  Master reference version of this file can be found in the at microserve/devops/new-project-template.

# The project ID as known to the QA controller for this project.
PROJECT_ID="661a2a05-564b-4a2e-add5-5557319c6229"

# The user to use when taking a database clone.
DATABASE_SSH_USERNAME="root"

# The host to use when taking a database clone.
DATABASE_SSH_HOST="umami.demo.microserve.io"

# The drush alias to use when taking a database clone.
DATABASE_SSH_ALIAS="@umamu"

# The drush command to use when taking a database clone.
DATABASE_DRUSH_COMMAND="drush9"
