<?php
namespace Deployer;

require 'recipe/drupal8.php';

// Project name
set('application', 'my_project');

// Project repository
set('repository', 'git@gitlab.microserve.io:microserve/demos/umami.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', ['docroot/sites/default/files']);

// Writable dirs by web server 
add('writable_dirs', ['docroot/sites/default/files']);

task('release', [
    'deploy:prepare',
    'deploy:release',
    'upload',
    'deploy:shared',
    'deploy:writable',
    'deploy:cim',
    'deploy:symlink',
]);

task('deploy', [
    'build',
    'release',
    'cleanup',
    'success'
]);

// Hosts

host('umami.demo.microserve.io')
    ->set('deploy_path', '/var/www/umami.demo.microserve.io/')
    ->set('stage', 'production')
        ->user('root');    
    
// Tasks
task('build', function () {
    run('composer install --no-dev --ignore-platform-reqs');
})->local();

task('upload', function () {
    upload(__DIR__ . "/", '{{release_path}}');
    run("chown www-data:www-data -R {{release_path}}");
});

task('deploy:cim', function() {
    run("cd {{ release_path }} && php7.2 vendor/drush/drush/drush cim");
    run("cd {{ release_path }} && php7.2 vendor/drush/drush/drush cr");
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

