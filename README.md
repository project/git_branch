# <Project Name>
## What belongs in this README?
For a complete overview of what does and does not belong in this README (and where to put any information that doesn't) please see the handbook page:

https://handbook.microserve.io/development/what-belongs-readme

## Setting up the project locally

<If your project has any special setup requirements add them here, otherwise remove this text and leave the below>

This project follows the standard microserve template and can be setup with the instructions located in the handbook at:

TODO: Put handbook link here.

## Deployment Process

<If this project has any special deployment processes, document them here. Otherwise remove this text and leave the below.>

This project follows the standard microserve deployment process which is documented at:

TODO: Put handbook link here.

## Contrib Modules

### Standard

TODO: Document standard contrib modules as outlined in issue #5 of new-project-template

### Custom

<Document all of the non-standard contrib modules and their purposes using the template below >

| module-name | Purpose | Link |
| ------ | ------ | ------ |
| module-1 | to do something | https://drupal.org/module1 |

## Known 'Gotchas'

<If there are any known gotchas for this project that new developers need to know they should be documented here. Examples could include:

Things that a developer may find a bit alarming at first, but we work around it as an ‘it is what it is’ type situation

Steps that we need to omit or add to a normal development or deployment workflow which would be unusual

Tools and techniques which we know are incompatible with the website and therefore cannot be used

If there are none, leave the below text as-is>

There are no currently known gotchas for this site.

## Third party integrations

<This section should give an overview of each 3rd party integration on the website, and should provide information such as who developed the integration, and who can Microserve contact within the client’s team (or elsewhere), regarding development or bugs with the integration. If there are no third party integrations leave the below text as-is>

There are currently no third party integrations used on this site.

## Other information

<This section should contain anything else missing above.

Any documentation that sits outside of the codebase should be linked to or attached as a file within this section. If there is no other information then leave the below text as-is>

There is currently no further documentation for this site.
