/**
 * @file
 * Provides condition values for all user fields conditions.
 */

(function (Drupal) {

  Drupal.smartContent = Drupal.smartContent || {};
  Drupal.smartContent.plugin = Drupal.smartContent.plugin || {};
  Drupal.smartContent.plugin.Field = Drupal.smartContent.plugin.Field || {};

  Drupal.smartContent.plugin.Field['user_fields:service_user'] = function (condition) {
    if ('user_fields' in drupalSettings && 'service_user' in drupalSettings.user_fields) {
      return drupalSettings.user_fields['service_user'] || false;
    }
    return false;
  };

})(Drupal);
