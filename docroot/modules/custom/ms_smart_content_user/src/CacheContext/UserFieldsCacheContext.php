<?php

namespace Drupal\ms_smart_content_user\CacheContext;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\user\Entity\User;

/**
 * Class TrackerCacheContext.
 */
class UserFieldsCacheContext implements CacheContextInterface {

  /**
   * Drupal\Core\TempStore\PrivateTempStoreFactory definition.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempstorePrivate;


  /**
   * Constructs a new TrackerCacheContext object.
   */
  public function __construct(PrivateTempStoreFactory $tempstore_private) {
    //$this->tempstorePrivate = $tempstore_private->get('ms_tracking.tracker');
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    \Drupal::messenger()->addMessage('Label of cache context');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    // Actual logic of context variation will lie here.
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    $value = FALSE;
    if ($user->hasField('field_service_user')) {
      $value = $user->get('field_service_user')->value ? TRUE : FALSE;
    }
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
