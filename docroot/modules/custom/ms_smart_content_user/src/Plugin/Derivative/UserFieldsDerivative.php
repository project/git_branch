<?php

namespace Drupal\ms_smart_content_user\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Deriver for BrowserCondition.
 *
 * Provides a deriver for
 * Drupal\smart_content_browser\Plugin\smart_content\Condition\BrowserCondition.
 * Definitions are based on properties available in JS from user's browser.
 */
class UserFieldsDerivative extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [
      'service_user' => [
        'label' => 'Service user',
        'type' => 'boolean',
      ] + $base_plugin_definition,
    ];
    return $this->derivatives;
  }

}
