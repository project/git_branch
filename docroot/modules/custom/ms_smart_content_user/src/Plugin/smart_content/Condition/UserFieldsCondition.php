<?php

namespace Drupal\ms_smart_content_user\Plugin\smart_content\Condition;

use Drupal\smart_content\Condition\ConditionTypeConfigurableBase;

/**
 * Provides a default Smart Condition.
 *
 * @SmartCondition(
 *   id = "user_fields",
 *   label = @Translation("User fields"),
 *   group = "user_fields",
 *   weight = 0,
 *   deriver = "Drupal\ms_smart_content_user\Plugin\Derivative\UserFieldsDerivative"
 * )
 */
class UserFieldsCondition extends ConditionTypeConfigurableBase {

  /**
   * {@inheritdoc}
   */
  public function getLibraries() {
    $libraries = array_unique(array_merge(parent::getLibraries(), ['ms_smart_content_user/condition.user_fields']));
    return $libraries;
  }

}
