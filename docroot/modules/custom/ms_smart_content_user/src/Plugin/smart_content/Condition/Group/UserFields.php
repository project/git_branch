<?php

namespace Drupal\ms_smart_content_user\Plugin\smart_content\Condition\Group;

use Drupal\smart_content\Condition\Group\ConditionGroupBase;

/**
 * Provides a default Smart Condition.
 *
 * @SmartConditionGroup(
 *   id = "user_fields",
 *   label = @Translation("User fields"),
 *   weight = 0,
 * )
 */
class UserFields extends ConditionGroupBase {

}
