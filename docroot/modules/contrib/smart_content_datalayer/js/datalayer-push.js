/**
 * @file
 * Drupal behavior to push smart content winner details to the dataLayer.
 */

(function (Drupal) {
  window.addEventListener('smart_content_decision', function (event) {
    // Format data to push into the data layer.
    let winnerDetails = event.detail.settings.dataLayer[event.detail.winner];
    let data = {
      decision_block: winnerDetails.decision_block,
      reaction_block: winnerDetails.reaction_block,
    };

    // Push or append data to the dataLayer.
    let index = window.dataLayer.findIndex(function (object) {
      return object.hasOwnProperty('smart_content');
    });
    window.dataLayer.push({
      'smart_content': data,
    });

  });

})(Drupal);
