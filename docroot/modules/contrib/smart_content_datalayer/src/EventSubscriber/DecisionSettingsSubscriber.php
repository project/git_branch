<?php

namespace Drupal\smart_content_datalayer\EventSubscriber;

use Drupal\smart_content\Decision\Storage\DecisionStorageManager;
use Drupal\smart_content\Event\AttachDecisionSettingsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes to attaching settings to decisions.
 *
 * @package Drupal\smart_content_datalayer\EventSubscriber
 */
class DecisionSettingsSubscriber implements EventSubscriberInterface {

  /**
   * The decision storage plugin manager.
   *
   * @var \Drupal\smart_content\Decision\Storage\DecisionStorageManager
   */
  protected $decisionStorageManager;

  /**
   * DecisionSettingsSubscriber constructor.
   *
   * @param \Drupal\smart_content\Decision\Storage\DecisionStorageManager $decisionStorageManager
   *   The decision storage plugin manager service.
   */
  public function __construct(DecisionStorageManager $decisionStorageManager) {
    $this->decisionStorageManager = $decisionStorageManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    return [
      AttachDecisionSettingsEvent::EVENT_NAME => 'addCustomSettings',
    ];
  }

  /**
   * Event handler to attach custom settings to decisions.
   *
   * @param \Drupal\smart_content\Event\AttachDecisionSettingsEvent $event
   *   The event object for the subscribed event.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function addCustomSettings(AttachDecisionSettingsEvent $event) {
    $settings =& $event->getSettings();
    $settings['dataLayer'] = [];

    $decisions = $settings['decisions'];
    foreach ($decisions as $decision) {
      // Load the correct decision storage plugin.
      /** @var \Drupal\smart_content\Decision\Storage\DecisionStorageInterface $decision_storage */
      $decision_storage = $this->decisionStorageManager->createInstance($decision['storage']);

      // Make sure the entity is loaded.
      $decision_storage->loadDecisionFromToken($decision['token']);
      if ($decision_storage->hasDecision()) {
        /** @var \Drupal\smart_content\Decision\DecisionInterface $decision_plugin */
        $decision_plugin = $decision_storage->getDecision();

        if ($decision_plugin) {
          $block = $this->getDecisionsBlock($decision_storage->getEntity()->id());
          $block_label = $block->get('settings')['label'];

          // Load the segments from the decision.
          $segments = $decision_plugin->getSegmentSetStorage()->getSegmentSet()->getSegments();
          foreach ($segments as $segment) {
            $settings['dataLayer'][$segment->getUuid()] = [
              'segment' => $segment->getUuid(),
              'reactions' => [],
            ];
          }
          // Get the reaction plugins and get each one's summary.
          foreach ($decision['reactions'] as $segment_id) {
            /** @var \Drupal\smart_content\Reaction\ReactionInterface $reaction */
            $reaction = $decision_plugin->getReaction($segment_id);
            $settings['dataLayer'][$segment_id]['reaction_block'] = $reaction->getPlainTextSummary();
            $settings['dataLayer'][$segment_id]['decision_block'] = $block_label;
          }
        }
      }
    }
  }

  /**
   * Helper to load the decision block in question.
   *
   * TODO need to remove this from this module to so it's not a hack.
   */
  private function getDecisionsBlock($decision_storage_id) {
    $storage = \Drupal::entityTypeManager()->getStorage('block');
    $block_ids = $storage->getQuery()->execute();
    $blocks = $storage->loadMultiple($block_ids);
    foreach ($blocks as $block) {
      $plugin = $block->getPlugin();
      if (method_exists($plugin, 'getDecisionStorage')) {
        if ($decision_storage_id == $plugin->getDecisionStorage()->getEntity()->id()) {
          return $block;
        }
      }
    }
  }

}
