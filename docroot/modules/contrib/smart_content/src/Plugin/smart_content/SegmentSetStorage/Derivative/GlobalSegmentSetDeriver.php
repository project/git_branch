<?php

namespace Drupal\smart_content\Plugin\smart_content\SegmentSetStorage\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\smart_content\Entity\SegmentSetConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides condition plugin definitions for Demandbase fields.
 *
 * @see Drupal\smart_content_demandbase\Plugin\smart_content\Condition\DemandbaseCondition
 */
class GlobalSegmentSetDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * DemandbaseConditionDeriver constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];
    $entities = SegmentSetConfig::loadMultiple();
    foreach ($entities as $entity) {
      $definition = [
        'label' => $entity->label(),
        'group' => $base_plugin_definition['label'],
      ];
      $this->derivatives[$entity->id()] = $definition + $base_plugin_definition;
    }
    return $this->derivatives;
  }

}
