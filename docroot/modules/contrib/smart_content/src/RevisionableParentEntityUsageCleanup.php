<?php

namespace Drupal\smart_content;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\smart_content\Decision\Storage\RevisionableParentEntityUsageInterface;

/**
 * Defines a class for reacting to entity events related to Inline Blocks.
 *
 * @internal
 *   This is an internal utility class wrapping hook implementations.
 */
class RevisionableParentEntityUsageCleanup {

  /**
   * Handles entity tracking on deleting a parent entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The parent entity.
   */
  public function handleDelete(EntityInterface $entity) {

    if (!$entity instanceof ContentEntityInterface) {
      return;
    }

    $definitions = \Drupal::service('plugin.manager.smart_content.decision_storage')
      ->getDefinitions();
    foreach ($definitions as $definition) {
      if (in_array(RevisionableParentEntityUsageInterface::class, class_implements($definition['class']))) {
        $definition['class']::deleteByParent($entity);
      }
    }
  }

}
