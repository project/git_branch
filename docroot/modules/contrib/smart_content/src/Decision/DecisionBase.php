<?php

namespace Drupal\smart_content\Decision;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Plugin\ObjectWithPluginCollectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\smart_content\Decision\Storage\DecisionStorageInterface;
use Drupal\smart_content\Event\AttachDecisionSettingsEvent;
use Drupal\smart_content\Reaction\ReactionInterface;
use Drupal\smart_content\Reaction\ReactionPluginCollection;
use Drupal\smart_content\SegmentSetStorage\SegmentSetStorageInterface;
use Drupal\smart_content\SegmentSetStorage\SegmentSetStoragePluginCollection;

// @todo: should we use Drupal\Core\Plugin\Factory\ContainerFactory

/**
 * Base class for ReactionSet storage plugins.
 */
abstract class DecisionBase extends PluginBase implements DecisionInterface, ConfigurableInterface, PluginFormInterface, ObjectWithPluginCollectionInterface {

  /**
   * An array of reaction settings.
   *
   * @var array
   */
  protected $reactions;

  /**
   * An array of segment storage settings.
   *
   * @var array
   */
  protected $segmentStorage;

  /**
   * A Uuid representing a unique instance of this plugin.
   *
   * @var string
   */
  protected $token;

  /**
   * The plugin ID of the decision storage.
   *
   * @var string
   */
  protected $storageId;

  /**
   * The plugin collection that holds the block plugin for this entity.
   *
   * @var \Drupal\smart_content\Reaction\ReactionPluginCollection
   */
  protected $reactionCollection;


  /**
   * The plugin collection that holds the block plugin for this entity.
   *
   * @var \Drupal\smart_content\Reaction\ReactionPluginCollection
   */
  protected $segmentStorageCollection;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);

    // If no token is set, generate a new one.
    if (!$this->hasToken()) {
      $this->refreshToken();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getReactions() {
    return $this->getReactionPluginCollection();
  }

  /**
   * {@inheritdoc}
   */
  public function getReaction($id) {
    return $this->getReactionPluginCollection()->get($id);
  }

  /**
   * {@inheritdoc}
   */
  public function hasReaction($id) {
    return $this->getReactionPluginCollection()->has($id);
  }

  /**
   * {@inheritdoc}
   */
  public function setReaction($instance_id, ReactionInterface $reaction) {
    $this->getReactionPluginCollection()->set($instance_id, $reaction);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function appendReaction(ReactionInterface $reaction) {
    $this->getReactionPluginCollection()->add($reaction);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeReaction($id) {
    $this->getReactionPluginCollection()->removeInstanceId($id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'reactions' => $this->getReactionPluginCollection(),
      'segmentStorage' => $this->getReactionPluginCollection(),
    ];
  }

  /**
   * Encapsulates the creation of the reactions's LazyPluginCollection.
   *
   * @return \Drupal\smart_content\Reaction\ReactionPluginCollection
   *   The reaction's plugin collection.
   */
  protected function getReactionPluginCollection() {
    if (!$this->reactionCollection) {
      $this->reactionCollection = new ReactionPluginCollection(\Drupal::service('plugin.manager.smart_content.reaction'),
        (array) $this->reactions);
    }
    return $this->reactionCollection;
  }

  /**
   * Encapsulates the creation of the segment storage's LazyPluginCollection.
   *
   * @return \Drupal\smart_content\Reaction\ReactionPluginCollection
   *   The block's plugin collection.
   */
  protected function getSegmentStoragePluginCollection() {
    if (!$this->segmentStorageCollection && isset($this->segmentStorage['id'])) {
      $this->segmentStorageCollection = new SegmentSetStoragePluginCollection(\Drupal::service('plugin.manager.smart_content.segment_set_storage'), $this->segmentStorage['id'], (array) $this->segmentStorage);
    }
    return $this->segmentStorageCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'id' => $this->getPluginId(),
      'segments_storage' => [],
      'reactions' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    $configuration = $this->defaultConfiguration();
    // @todo:  I might need to determine if these even exist first?
    if ($segment_collection = $this->getSegmentStoragePluginCollection()) {
      $configuration['segments_storage'] = $segment_collection->getConfiguration();
    }
    if ($reaction_collection = $this->getReactionPluginCollection()) {
      $configuration['reactions'] = $reaction_collection->getConfiguration();
    }
    $configuration['token'] = $this->getToken();
    $configuration['storage_id'] = $this->getStorageId();
    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $configuration = $configuration + $this->defaultConfiguration();

    if (isset($configuration['segments_storage'])) {
      $this->segmentStorage = (array) $configuration['segments_storage'];
      $this->segmentStorageCollection = NULL;
    }
    if (isset($configuration['reactions'])) {
      $this->reactions = (array) $configuration['reactions'];
      $this->reactionCollection = NULL;
    }

    if (isset($configuration['token'])) {
      $this->token = $configuration['token'];
    }
    if (isset($configuration['storage_id'])) {
      $this->storageId = $configuration['storage_id'];
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setSegmentSetStorage(SegmentSetStorageInterface $segment_set_storage) {
    $this->segmentStorage = $segment_set_storage->getConfiguration();
    $this->segmentStorageCollection = NULL;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSegmentSetStorage() {
    if ($this->getSegmentStoragePluginCollection()) {
      $instance_ids = $this->getSegmentStoragePluginCollection()
        ->getInstanceIds();
      return $this->getSegmentStoragePluginCollection()
        ->get(reset($instance_ids));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getResponse(ReactionInterface $reaction) {
    $response = new AjaxResponse();
    if ($this->hasReaction($reaction->getSegmentDependencyId())) {
      $response = $this->getReaction($reaction->getSegmentDependencyId())
        ->getResponse($this);
    }
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function attach(array $element) {
    if ($this->getSegmentSetStorage()) {
      if ($settings = $this->getAttachedSettings()) {
        $element['#attached'] = [
          'drupalSettings' => [
            'smartContent' => $this->getAttachedSettings(),
          ],
        ];

        if ($libraries = $this->getLibraries()) {
          $element['#attached']['library'] = $libraries;
        }
      }
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function getAttachedSettings() {
    $settings = [];
    if ($this->getStorageId() && $this->getSegmentSetStorage()) {
      $settings = $this->getSegmentSetStorage()->getAttachedSettings();
      $decision_settings = [
        'token' => $this->getToken(),
        'storage' => $this->getStorageId(),
      ];
      foreach ($this->getReactions() as $reaction) {
        $reaction_id = $reaction->getSegmentDependencyId();
        if (isset($settings['segments'][$reaction_id])) {
          $decision_settings['reactions'][] = $reaction_id;
        }
      }
      $settings['decisions'][$this->getToken()] = $decision_settings;
    }
    // Dispatch an event so other modules can alter settings.
    \Drupal::service('event_dispatcher')->dispatch(AttachDecisionSettingsEvent::EVENT_NAME, new AttachDecisionSettingsEvent($settings));
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries() {
    $libraries = [];
    if ($storage = $this->getSegmentSetStorage()) {
      $libraries = $storage->getLibraries();
      $libraries[] = 'smart_content/smart_content';
    }
    return $libraries;
  }

  /**
   * {@inheritdoc}
   */
  public function refreshToken() {
    $this->token = \Drupal::service('uuid')->generate();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getToken() {
    return $this->token;

  }

  /**
   * {@inheritdoc}
   */
  public function hasToken() {
    return isset($this->token);
  }

  /**
   * {@inheritdoc}
   */
  public function setStorage(DecisionStorageInterface $decision_storage) {
    $this->storageId = $decision_storage->getPluginId();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStorageId() {
    return $this->storageId;
  }

}
