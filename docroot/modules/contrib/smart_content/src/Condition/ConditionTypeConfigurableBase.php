<?php

namespace Drupal\smart_content\Condition;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ObjectWithPluginCollectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\smart_content\Condition\Type\ConditionTypePluginCollection;

/**
 * A base class for conditions using the type plugin.
 *
 * @package Drupal\smart_content\Condition
 */
abstract class ConditionTypeConfigurableBase extends ConditionBase implements PluginFormInterface, ObjectWithPluginCollectionInterface {

  /**
   * The plugin collection to lazy load the condition type plugin.
   *
   * @var \Drupal\smart_content\Condition\Type\ConditionTypeInterface
   */
  protected $conditionTypeCollection;

  /**
   * Helper function to return condition type.
   *
   * @return \Drupal\smart_content\Condition\Type\ConditionTypeInterface|object
   *   The condition type.
   */
  public function getConditionType() {
    return $this->getConditionTypePluginCollection()
      ->get($this->getPluginDefinition()['type']);
  }

  /**
   * Gets the plugin collections used by this object.
   *
   * @return \Drupal\Component\Plugin\LazyPluginCollection[]
   *   An array of plugin collections, keyed by the property name they use to
   *   store their configuration.
   */
  public function getPluginCollections() {
    return [
      'condition_type_settings' => $this->getConditionTypePluginCollection(),
    ];
  }

  /**
   * Encapsulates the creation of the conditions's LazyPluginCollection.
   *
   * @return \Drupal\smart_content\Condition\Type\ConditionTypePluginCollection
   *   The condition's type plugin collection.
   */
  protected function getConditionTypePluginCollection() {
    $plugin_type_definition = $this->getPluginDefinition()['type'];
    if (!$this->conditionTypeCollection) {
      $this->conditionTypeCollection = new ConditionTypePluginCollection(\Drupal::service('plugin.manager.smart_content.condition_type'),
        $plugin_type_definition,
        (array) $this->configuration['condition_type_settings'], $this);
    }
    return $this->conditionTypeCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = ConditionBase::attachNegateElement($form, $this->configuration);
    $form['#attributes']['class'] = ['condition'];
    $definition = $this->getPluginDefinition();
    $label = $definition['label'];
    if ($group_label = $this->getGroupLabel()) {
      $label .= '(' . $group_label . ')';
    }
    $form['label'] = [
      '#type' => 'container',
      '#markup' => $label,
      '#attributes' => ['class' => ['condition-label']],
    ];
    return $this->getConditionType()
      ->buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->getConditionType()->validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->setNegated($form_state->getValue('negate'));
    $this->getConditionType()->submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries() {
    return $this->getConditionType()->getLibraries();
  }

  /**
   * {@inheritdoc}
   */
  public function getAttachedSettings() {
    $settings = parent::getAttachedSettings();
    // Add the field 'settings' from the ConditionType Plugin.
    // @todo: do we need getFieldAttachedSettings() ?
    $settings['field']['settings'] = $this->getConditionType()
      ->getAttachedSettings();
    // Get the 'settings' from the ConditionType Plugin.
    $settings['settings'] = $this->getConditionType()->getAttachedSettings();
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'condition_type_settings' => $this->getConditionType()
        ->defaultConfiguration(),
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    $configuration = parent::getConfiguration();
    foreach ($this->getPluginCollections() as $plugin_config_key => $plugin_collection) {
      $configuration[$plugin_config_key] = $plugin_collection->getConfiguration();
    }
    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getTypeId() {
    return 'type:' . $this->getConditionType()->getPluginId();
  }

}
