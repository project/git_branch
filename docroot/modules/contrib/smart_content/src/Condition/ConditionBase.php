<?php

namespace Drupal\smart_content\Condition;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Smart condition plugins.
 */
abstract class ConditionBase extends PluginBase implements ConditionInterface, ConfigurableInterface {

  /**
   * Condition is/is not.
   *
   * @var bool
   */
  protected $negate;

  /**
   * The condition weight.
   *
   * @var int
   */
  protected $weight;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function getTypeId() {
    return 'plugin:' . $this->getPluginId();
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return isset($this->weight) ? $this->weight : 0;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->weight = $weight;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isNegated() {
    return isset($this->negate) ? $this->negate : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setNegated($value = TRUE) {
    $this->negate = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getAttachedSettings() {
    // TODO: Potentially add a 'field_key' setting for non_unique that require
    // field settings to determine uniqueness.
    $definition = $this->getPluginDefinition();
    $settings = [
      'field' => [
        'pluginId' => $this->getPluginId(),
        'type' => $this->getTypeId(),
        'negate' => $this->isNegated(),
        'unique' => $definition['unique'],
      ],
    ];
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'id' => $this->getPluginId(),
      'weight' => $this->getWeight(),
      'negate' => $this->isNegated(),
      'type' => $this->getTypeId(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return [
      'id' => $this->getPluginId(),
      'weight' => $this->getWeight(),
      'negate' => $this->isNegated(),
      'type' => $this->getTypeId(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $configuration = $configuration + $this->defaultConfiguration();

    if (isset($configuration['weight'])) {
      $this->weight = (int) $configuration['weight'];
    }
    if (isset($configuration['negate'])) {
      $this->negate = (bool) $configuration['negate'];
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return ['#markup' => 'todo condition'];
  }

  /**
   * Utility function to return the condition group label.
   *
   * @return string
   *   The group label.
   */
  public function getGroupLabel() {
    $group = $this->getPluginDefinition()['group'];
    // TODO: Add dependency injection.
    $conditionGroupManager = \Drupal::service('plugin.manager.smart_content.condition_group');
    if ($conditionGroupManager->hasDefinition($group)) {
      return $conditionGroupManager->getDefinition($group)['label'];
    }
    return '';
  }

  /**
   * Utility function to provide "If/If not" select element.
   *
   * @param array $form
   *   The form render array.
   * @param array $config
   *   The conditions configuration.
   *
   * @return array
   *   The form with negate attached.
   */
  public static function attachNegateElement(array $form, array $config) {
    $form['negate'] = [
      '#title' => 'Negate',
      '#title_display' => 'hidden',
      '#type' => 'select',
      '#default_value' => isset($config['negate']) ? $config['negate'] : FALSE,
      '#empty_option' => 'If',
      '#empty_value' => FALSE,
      '#options' => [TRUE => 'If Not'],
    ];
    return $form;
  }

}
