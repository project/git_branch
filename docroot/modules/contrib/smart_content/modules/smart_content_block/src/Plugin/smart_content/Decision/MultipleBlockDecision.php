<?php

namespace Drupal\smart_content_block\Plugin\smart_content\Decision;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\smart_content\Decision\DecisionBase;
use Drupal\smart_content\Decision\PlaceholderDecisionInterface;
use Drupal\smart_content\Form\SegmentSetConfigEntityForm;
use Drupal\smart_content\Plugin\smart_content\SegmentSetStorage\Inline;
use Drupal\smart_content\Segment;

/**
 * Provides a 'Multiple Block Decision' Decision plugin.
 *
 * @SmartDecision(
 *  id = "multiple_block_decision",
 *  label = @Translation("Multiple Block Decision"),
 * )
 */
class MultipleBlockDecision extends DecisionBase implements PlaceholderDecisionInterface {

  /**
   * The default segment uuid.
   *
   * @var string
   */
  protected $savedDefault;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = $this->buildSegmentSetSelectWidget($form, $form_state);
    $wrapper_id = 'decision-reaction-wrapper';

    $form['decision_settings'] = [
      '#type' => 'details',
      '#attributes' => [
        'id' => $wrapper_id,
        'class' => [
          'decision-settings-wrapper',
        ],
      ],
      '#title' => 'Configure Reactions',
    ];

    if ($this->getSegmentSetStorage()) {
      if ($this->getSegmentSetStorage() instanceof Inline) {
        if (count($this->getSegmentSetStorage()->getSegmentSet()->getSegments()) === 0) {
          $this->getSegmentSetStorage()
            ->getSegmentSet()
            ->setSegment(Segment::fromArray());
        }
      }
      foreach ($this->getSegmentSetStorage()->getSegmentSet()->getSegments() as $segment) {
        if ($this->getSegmentSetStorage() instanceof Inline) {
          if ($segment->getConditions()->count() === 0) {
            $segment->appendCondition(\Drupal::service('plugin.manager.smart_content.condition')
              ->createInstance('group'));
          }
        }
        if (!$this->hasReaction($segment->getUuid())) {
          $reaction = \Drupal::service('plugin.manager.smart_content.reaction')
            ->createInstance('display_blocks');
          $reaction->setSegmentDependency($segment);
          $this->appendReaction($reaction);
        }
      }

      if ($this->getSegmentSetStorage()->getPluginDefinition()['global']) {
        $form = $this->buildSelectedSegmentSetWidget($form, $form_state);
      }
      elseif ($this->getSegmentSetStorage() instanceof Inline) {
        $form = $this->buildInlineSegmentSetWidget($form, $form_state);
      }
    }

    return $form;
  }

  /**
   * A form for selecting a global segment set or using an inline segment set.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The altered form array.
   */
  public function buildSegmentSetSelectWidget(array &$form, FormStateInterface $form_state) {
    // @todo: Set default if no form value is set, but storage is set.
    // @todo: automatically assume inline if no other options.
    // @todo: add locking functionalitionality and warning on change.
    // @todo: if storage is inline, add checkbox to allow global save.
    $options = \Drupal::service('plugin.manager.smart_content.segment_set_storage')
      ->getFormOptions();

    $wrapper_id = 'decision-reaction-wrapper';
    $form['decision_select'] = [
      '#type' => 'details',
      '#title' => 'Segment settings',
      '#open' => TRUE,
    ];
    $form['decision_select']['list'] = [
      '#type' => 'select',
      '#title' => 'Segment Sets',
      '#title_display' => 'hidden',
      '#options' => $options,
      '#required' => TRUE,
      '#empty_option' => '-- Select a Segment Set --',
    ];

    $form['decision_select']['update'] = [
      '#type' => 'submit',
      '#value' => t('Select Segment Set'),
      '#submit' => [[$this, 'updateSegmentSet']],
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => [$this, 'updateSegmentSetAjax'],
        'wrapper' => $wrapper_id,
      ],
    ];

    // Check if a value was set for the decision select element already. If yes,
    // set a default value for the list field and disable it.
    if ($this->getSegmentSetStorage()) {
      $form['decision_select']['list']['#default_value'] = $this->getSegmentSetStorage()
        ->getPluginId();
      $form['decision_select']['list']['#disabled'] = TRUE;
      $form['decision_select']['update']['#access'] = FALSE;
    }

    return $form;
  }

  /**
   * Provides a '#submit' callback for adding a Variation.
   */
  public function updateSegmentSet(array &$form, FormStateInterface $form_state) {
    $parents = array_slice($form_state->getTriggeringElement()['#parents'],
      0, -1);
    $parents[] = 'list';

    $type = NestedArray::getValue($form_state->getUserInput(), $parents);
    $segment_set_storage = \Drupal::service('plugin.manager.smart_content.segment_set_storage')
      ->createInstance($type);
    $this->setSegmentSetStorage($segment_set_storage);
    $form_state->setRebuild();
  }

  /**
   * Provides an '#ajax' callback for adding a Variation.
   */
  public function updateSegmentSetAjax(array &$form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();
    // Go one level up in the form, to the widgets container.
    return NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -2))['decision_settings'];
  }

  /**
   * The inline segment set form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The altered form array.
   */
  public function buildInlineSegmentSetWidget(array &$form, FormStateInterface $form_state) {
    // @todo: Implement unique id for wrapper.
    $wrapper_id = 'decision-reaction-wrapper';

    $form['decision_settings'] = [
      '#type' => 'fieldset',
      '#attributes' => [
        'id' => $wrapper_id,
        'class' => [
          'decision-settings-wrapper',
        ],
      ],
      '#title' => 'Configure Segments and Reactions',
    ];

    $form['decision_settings']['segments'] = [
      '#type' => 'table',
      '#header' => ['', t('Weight'), ''],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => $wrapper_id . '-order-weight',
        ],
      ],
    ];

    $i = 0;

    $default = $this->getSegmentSetStorage()->getSegmentSet()->getDefaultSegment();

    foreach ($this->getSegmentSetStorage()->getSegmentSet()->getSegments() as $uuid => $segment) {
      $form['decision_settings']['segments'][$i]['#attributes']['class'][] = 'draggable';
      $form['decision_settings']['segments'][$i]['settings'] = [
        '#type' => 'fieldset',
        '#title' => 'Segment ' . ($i + 1),
        '#attributes' => [
          'class' => [
            'segment-settings-wrapper',
          ],
        ],
      ];

      foreach ($segment->getConditions() as $ii => $condition) {
        SegmentSetConfigEntityForm::pluginForm($condition, $form, $form_state, [
          'decision_settings',
          'segments',
          $i,
          'settings',
          'condition_settings',
          $ii,
          'plugin_form',
        ]);
      }
      $form['decision_settings']['segments'][$i]['settings']['condition_settings']['#type'] = 'fieldset';
      $form['decision_settings']['segments'][$i]['settings']['condition_settings']['#attributes'] = [
        'class' => [
          'condition-settings-wrapper',
        ],
      ];

      $reaction = $this->getReaction($segment->getUuid());
      SegmentSetConfigEntityForm::pluginForm($reaction, $form, $form_state, [
        'decision_settings',
        'segments',
        $i,
        'settings',
        'reaction_settings',
        'plugin_form',
      ]);

      $form['decision_settings']['segments'][$i]['settings']['reaction_settings']['#type'] = 'fieldset';
      $form['decision_settings']['segments'][$i]['settings']['reaction_settings']['#attributes'] = [
        'class' => [
          'reaction-settings-wrapper',
        ],
      ];

      $disabled = ($default && $default->getUuid() != $segment->getUuid()) ? 'disabled' : '';

      $form['decision_settings']['segments'][$i]['settings']['additional_settings'] = [
        '#type' => 'container',
        '#weight' => 10,
        '#attributes' => [
          'class' => ['segment-additional-settings-container'],
          'disabled' => [$disabled],
        ],
      ];

      $form['decision_settings']['segments'][$i]['settings']['additional_settings']['default'] = [
        '#type' => 'checkbox',
        '#attributes' => [
          'class' => ['smart-variations-default-' . $segment->getUuid()],
          'disabled' => [$disabled],
        ],
        '#title' => 'Set as default segment',
        '#default_value' => $segment->isDefault(),
      ];

      $form['decision_settings']['segments'][$i]['weight'] = [
        '#type' => 'weight',
        '#title' => 'Weight',
        '#title_display' => 'invisible',
        '#attributes' => ['class' => [$wrapper_id . '-order-weight']],
      ];

      $form['decision_settings']['segments'][$i]['remove_variation'] = [
        '#type' => 'submit',
        '#value' => t('Remove'),
        '#name' => 'remove_variation__' . $uuid,
        '#submit' => [[$this, 'removeElementSegment']],
        '#attributes' => [
          'class' => [
            'align-right',
            'remove-variation',
            'remove-button',
          ],
        ],
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => [$this, 'removeElementSegmentAjax'],
          'wrapper' => $wrapper_id,
        ],
      ];
      $i++;
    }
    // @todo: Trigger modal to configure this before saving.
    $form['decision_settings']['add_segment'] = [
      '#type' => 'submit',
      '#value' => t('Add Another Segment'),
      '#submit' => [[$this, 'addElementSegment']],
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => [$this, 'addElementSegmentAjax'],
        'wrapper' => $wrapper_id,
      ],
    ];

    $form['#attached']['library'][] = 'smart_content/form';
    return $form;
  }

  /**
   * Provides a '#submit' callback for adding a Variation.
   */
  public function addElementSegment(array &$form, FormStateInterface $form_state) {
    $parents = array_slice($form_state->getTriggeringElement()['#parents'], 0, -2);
    // Map segment weights.
    $values = NestedArray::getValue($form_state->getUserInput(), $parents);
    $this->mapFormSegmentWeights($values);
    // todo: $form_state->storage() refreshes on first ajax submit on layout
    // builder, so we may consider using form_values to append additional
    // segments.
    $segment = Segment::fromArray();
    $this->getSegmentSetStorage()->getSegmentSet()->setSegment($segment);

    $form_state->setRebuild();
  }

  /**
   * Provides an '#ajax' callback for adding a Variation.
   */
  public function addElementSegmentAjax(array &$form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();
    // Go one level up in the form, to the widgets container.
    return NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -1));
  }

  /**
   * Provides a '#submit' callback for removing a Variation.
   */
  public function removeElementSegment(array &$form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();
    $parents = array_slice($button['#parents'], 0, -4);
    // Map segment weights.
    $values = NestedArray::getValue($form_state->getUserInput(), $parents);
    $this->mapFormSegmentWeights($values);

    list($action, $name) = explode('__', $button['#name']);
    // @todo: Fix issue with changing UUID causing issues here.
    $this->getSegmentSetStorage()->getSegmentSet()->removeSegment($name);
    $form_state->setRebuild();
  }

  /**
   * Provides an '#ajax' callback for removing a Variation.
   */
  public function removeElementSegmentAjax(array &$form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();
    // Go one level up in the form, to the widgets container.
    return NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -3));
  }

  /**
   * Maps the block weight values to the block collection.
   *
   * @param array $values
   *   The base form values or input.
   */
  public function mapFormSegmentWeights(array $values) {
    $i = 0;
    foreach ($this->getSegmentSetStorage()->getSegmentSet()->getSegments() as $segment) {
      $segment->setWeight((int) $values['decision_settings']['segments'][$i]['weight']);
      $i++;
    }
    $this->getSegmentSetStorage()->getSegmentSet()->sortSegments();
  }

  /**
   * The selected segment set form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The altered form array.
   */
  public function buildSelectedSegmentSetWidget(array &$form, FormStateInterface $form_state) {
    // @todo: Implement unique id for wrapper.
    $wrapper_id = 'decision-reaction-wrapper';

    $form['decision_settings'] = [
      '#type' => 'fieldset',
      '#attributes' => [
        'id' => $wrapper_id,
        'class' => [
          'decision-settings-wrapper',
        ],
      ],
      '#title' => 'Configure Reactions',
    ];

    $i = 0;
    foreach ($this->getSegmentSetStorage()->getSegmentSet()->getSegments() as $segment_id => $segment) {

      $form['decision_settings']['segments'][$i] = [
        '#type' => 'fieldset',
        '#title' => 'Segment ' . ($i + 1),
      ];

      if ($segment->isDefault()) {
        $form['decision_settings']['segments'][$i]['settings']['default'] = [
          '#type' => 'markup',
          '#markup' => 'Default',
        ];
      }

      $reaction = $this->getReaction($segment->getUuid());
      SegmentSetConfigEntityForm::pluginForm($reaction, $form, $form_state, [
        'decision_settings',
        'segments',
        $i,
        'settings',
        'reaction_settings',
        'plugin_form',
      ]);

      $i++;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $is_inline = $this->getSegmentSetStorage() instanceof Inline;
    $i = 0;
    if (!$this->getSegmentSetStorage()) {
      $form_state->setError($form['decision_select'], 'You must first select a Segment Set.');
    }
    else {
      foreach ($this->getSegmentSetStorage()->getSegmentSet()->getSegments() as $uuid => $segment) {
        if ($is_inline) {
          foreach ($segment->getConditions() as $condition_id => $condition) {
            SegmentSetConfigEntityForm::pluginFormValidate($condition, $form, $form_state, [
              'decision_settings',
              'segments',
              $i,
              'settings',
              'condition_settings',
              $condition_id,
              'plugin_form',
            ]);
          }
        }
        $reaction = $this->getReaction($segment->getUuid());
        SegmentSetConfigEntityForm::pluginFormValidate($reaction, $form, $form_state, [
          'decision_settings',
          'segments',
          $i,
          'settings',
          'reaction_settings',
          'plugin_form',
        ]);
        $i++;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $is_inline = $this->getSegmentSetStorage() instanceof Inline;
    $i = 0;

    $has_default = FALSE;
    foreach ($this->getSegmentSetStorage()->getSegmentSet()->getSegments() as $uuid => $segment) {
      if ($is_inline) {
        if ($values['decision_settings']['segments'][$i]['settings']['additional_settings']['default']) {
          $has_default = TRUE;
          $this->getSegmentSetStorage()
            ->getSegmentSet()
            ->setDefaultSegment($segment->getUuid());
        }
        foreach ($segment->getConditions() as $condition_id => $condition) {
          SegmentSetConfigEntityForm::pluginFormSubmit($condition, $form, $form_state, [
            'decision_settings',
            'segments',
            $i,
            'settings',
            'condition_settings',
            $condition_id,
            'plugin_form',
          ]);
        }
      }

      $reaction = $this->getReaction($segment->getUuid());
      SegmentSetConfigEntityForm::pluginFormSubmit($reaction, $form, $form_state, [
        'decision_settings',
        'segments',
        $i,
        'settings',
        'reaction_settings',
        'plugin_form',
      ]);
      $i++;
    }
    if ($is_inline && !$has_default && $this->getSegmentSetStorage()) {
      $this->getSegmentSetStorage()->getSegmentSet()->unsetDefaultSegment();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPlaceholderId() {
    return 'decision-block-' . $this->getToken();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['default' => ''] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    parent::setConfiguration($configuration);
    if (isset($configuration['default'])) {
      $this->savedDefault = $configuration['default'];
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    $configuration = parent::getConfiguration();
    if ($this->getSegmentSetStorage()) {
      if ($default_segment = $this->getSegmentSetStorage()
        ->getSegmentSet()
        ->getDefaultSegment()) {
        $configuration['default'] = $default_segment->getUuid();
      }
    }
    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getAttachedSettings() {
    $settings = parent::getAttachedSettings();
    if ($default_segment = $this->getSegmentSetStorage()->getSegmentSet()->getDefaultSegment()) {
      if (in_array($default_segment->getUuid(), $settings['decisions'][$this->getToken()]['reactions'])) {
        $settings['decisions'][$this->getToken()]['default'] = $default_segment->getUuid();
      }
      elseif ($this->savedDefault) {
        $settings['decisions'][$this->getToken()]['default'] = $this->savedDefault;
      }
    }
    return $settings;
  }

}
