<?php

namespace Drupal\smart_content_block\Plugin\Block;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\PreviewFallbackInterface;
use Drupal\smart_content\Decision\Storage\DecisionStorageBase;
use Drupal\smart_content\Form\SegmentSetConfigEntityForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'SmartBlock' block.
 *
 * @todo: omit this block from layout_builder and potentially block_field.
 *
 * @Block(
 *  id = "smart_content_decision_block",
 *  admin_label = @Translation("Decision Block"),
 * )
 */
class DecisionBlock extends BlockBase implements ContainerFactoryPluginInterface, PreviewFallbackInterface {

  /**
   * The decision storage.
   *
   * @var mixed
   */
  protected $decisionStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    if (isset($configuration['decision_storage_serialized'])) {
      $this->decisionStorage = unserialize($configuration['decision_storage_serialized']);
      unset($configuration['decision_storage_serialized']);
    }
    if (!$this->getDecisionStorage()->hasDecision()) {
      $decision_stub = \Drupal::service('plugin.manager.smart_content.decision')
        ->createInstance('multiple_block_decision');
      $this->getDecisionStorage()->setDecision($decision_stub);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label_display' => FALSE,
      'decision_storage' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $decision = $this->getDecisionStorage()->getDecision();

    $build = [
      '#attributes' => ['data-smart-content-placeholder' => $decision->getPlaceholderId()],
      '#markup' => ' ',
    ];

    $build = $decision->attach($build);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $form['#process'][] = [$this, 'buildWidget'];
    return $form;
  }

  /**
   * Render API callback: builds the formatter settings elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   * @param array $complete_form
   *   The complete form array.
   *
   * @return array
   *   The processed form element.
   */
  public function buildWidget(array &$element, FormStateInterface $form_state, array &$complete_form) {
    if ($decision_storage = DecisionStorageBase::getWidgetState($element['#parents'], $form_state)) {
      $this->decisionStorage = $decision_storage;
    }
    DecisionStorageBase::setWidgetState($element['#parents'], $form_state, $this->getDecisionStorage());

    SegmentSetConfigEntityForm::pluginForm($this->getDecisionStorage()
      ->getDecision(), $element, $form_state, ['decision']);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $parents = $form['#parents'];

    if ($decision_storage = DecisionStorageBase::getWidgetState($parents, $form_state)) {
      $this->decisionStorage = $decision_storage;
    }

    if ($this->getDecisionStorage()->getDecision()) {
      SegmentSetConfigEntityForm::pluginFormValidate($this->getDecisionStorage()
        ->getDecision(), $form, $form_state, ['decision']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $parents = $form['#parents'];
    $parents[] = 'settings';

    if ($decision_storage = DecisionStorageBase::getWidgetState($parents, $form_state)) {
      // TODO: Outdated?
      $this->decisionStorage = $decision_storage;
    }
    $element = NestedArray::getValue($form, $parents);

    SegmentSetConfigEntityForm::pluginFormSubmit($this->getDecisionStorage()
      ->getDecision(), $element, $form_state, ['decision']);

    // todo: explain this and how to make smart_blocks work with other modules.
    $this->setConfigurationValue('decision_storage_serialized', serialize($this->getDecisionStorage()));
  }

  /**
   * Get the decision storage plugin.
   *
   * @return \Drupal\smart_content\Decision\Storage\DecisionStorageInterface
   *   The decision storage plugin.
   */
  public function getDecisionStorage() {
    if (!isset($this->decisionStorage)) {
      $decision_storage_configuration = $this->getConfiguration()['decision_storage'];
      $storage_plugin_id = isset($decision_storage_configuration['plugin_id']) ? $decision_storage_configuration['plugin_id'] : 'config_entity';
      $this->decisionStorage = \Drupal::service('plugin.manager.smart_content.decision_storage')
        ->createInstance($storage_plugin_id, (array) $decision_storage_configuration);
    }
    return $this->decisionStorage;
  }

  /**
   * Saves the block_content entity for this plugin.
   */
  public function saveBlockContent() {
    $this->getDecisionStorage()->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getPreviewFallbackString() {
    return $this->t('Placeholder for Smart Content Decision Block');
  }

}
