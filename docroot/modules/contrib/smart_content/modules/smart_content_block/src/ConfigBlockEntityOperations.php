<?php

namespace Drupal\smart_content_block;

use Drupal\block\Entity\Block;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a class for reacting to entity events related to Inline Blocks.
 *
 * @internal
 *   This is an internal utility class wrapping hook implementations.
 */
class ConfigBlockEntityOperations {

  /**
   * Handles entity tracking on deleting a parent entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The parent entity.
   */
  public function handlePreDelete(EntityInterface $entity) {

    if (!$this->isConfigBlockEntity($entity)) {
      return;
    }

    $configuration = $entity->get('settings');
    $decision_storage_configuration = $configuration['decision_storage'];
    $decision_storage = \Drupal::service('plugin.manager.smart_content.decision_storage')
      ->createInstance($decision_storage_configuration['plugin_id'], $decision_storage_configuration);
    $decision_storage->delete();
  }

  /**
   * Handles saving a parent entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The parent entity.
   */
  public function handlePreSave(EntityInterface $entity) {
    if (!$this->isConfigBlockEntity($entity)) {
      return;
    }
    $configuration = $entity->get('settings');
    if (isset($configuration['decision_storage_serialized'])) {
      $decision_storage = unserialize($configuration['decision_storage_serialized']);
      $decision_storage->save();
      $configuration['decision_storage'] = $decision_storage->getConfiguration();
      unset($configuration['decision_storage_serialized']);
      $entity->set('settings', $configuration);
    }
  }

  /**
   * Helper function to check if entity is type decision block.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   Is a decision block.
   */
  public function isConfigBlockEntity(EntityInterface $entity) {
    return ($entity instanceof Block && $entity->getPluginId() == 'smart_content_decision_block');
  }

}
