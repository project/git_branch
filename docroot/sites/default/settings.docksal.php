<?php

/**
 * @file
 * Docksal specific settings file.
 */

$databases['default']['default'] = [
  'driver' => 'mysql',
  'database' => getenv('MYSQL_DATABASE'),
  'username' => getenv('MYSQL_USER'),
  'password' => getenv('MYSQL_PASSWORD'),
  'host' => 'db',
  'port' => '3306',
  'prefix' => '',
  'charset' => 'utf8mb4',
  'collation' => 'utf8mb4_general_ci',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
];

$settings['trusted_host_patterns'] = [
  '^.*\.docksal$',
  '^.*\.microserve-qa\.io'
];

$settings['hash_salt'] = file_get_contents($app_root . '/../hash_salt.txt');